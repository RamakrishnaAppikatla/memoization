import React from "react";

const Memoization = () => {

  // function that takes a function and returns a function
  const memoize = (func) => {
    // a cache of results
    const results = {};
    // return a function for the cache of results
    return (...args) => {
      // a JSON key to save the results cache
      const argsKey = JSON.stringify(args);
      // execute `func` only if there is no cached value
      if (!results[argsKey]) {
        // store the return value
        results[argsKey] = func(...args);
      }
      console.log(results);
      // return the cached results
      return results[argsKey];
    };
  };

  const normalSquare = memoize((num) => {
    let result = 0;
    for (let i = 1; i <= num; i++) {
      for (let j = 1; j <= num; j++) {
        result++;
      }
    }
    return result;
  });

  const square = (num) => {
    for (var i = 0; i < 3; i++) {
      console.time('call---'+(i+1));
      normalSquare(num);
      console.timeEnd('call---'+(i+1));
    }
  };

  const fibonacci = (n) => {
    if (n == 1) {
      return 1;
    }
    else if (n == 2) {
      return 1;
    }
    else return fibonacci(n - 1) + fibonacci(n - 2);
  };

  const fib = (num) => {
    console.time("Fibonacci");
    console.log(fibonacci(num));
    console.timeEnd("Fibonacci");
  };
  return (
    <div>
      <input type="text" onChange={(e)=>fib(e?.target?.value)}/>
    </div>
  );
};

export default Memoization;
